# README #
## additional data needed ##
### Corpus ###
a (German) corpus belongs in the 'corpusScripts' directory. You need at least 10 times the space of the size of the corpus to run the scripts and have to adjust the temp-directory for the sort command (about 5 times the corpus space should be available for sorting there)
### Weight matrix ###
a weight matrix for German words and trigrams belongs in the 'evaluation' directory

## update paths ##
It might be necessary to change the paths used in our files according to your setup.

## time ##
Even though we had put some effort in making the calculations faster they may take some time - depending on your system.
It's hard to tell how much time is needed in the current state because we did the calculations step-by-step.
