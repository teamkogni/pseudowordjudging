#!/bin/bash

#clean corpus and extract words
dd if=$1 conv=lcase | sed 's/Ä/ä/g' | sed 's/Ü/ü/g' | sed 's/Ö/ö/g' | tr -d '#' | sed 's/ /##/g' | sed 's/ /##/g'  | sed 's/#.#//g' | sed 's/\([a-zäöüß]\)/#\1/' | sed 's/##/#\n#/g' | grep "^#[a-zäöüß]*#$" > "$1.words"

echo "corpus cleaned"

#corpus with each word only once
sort -u "$1.words" > "$1.single.words"

echo "words and single words produced"

#gets words (one line each) and return trigrams (one line each)
for (( i = 1; i < 4; i++ )); do
	tail -c +"$i" "$1.words" | sed 's/\([a-zäöüß#][a-zäöüß#][a-zäöüß#]\)/\1\n/g' >> "$1.words.trigrams" &
	tail -c +"$i" "$1.single.words" | sed 's/\([a-zäöüß#][a-zäöüß#][a-zäöüß#]\)/\1\n/g' >> "$1.single.words.trigrams" &
done

wait
echo "trigrams produced"

#clean trigrams
grep "^[a-zäöüß#][a-zäöüß][a-zäöüß#]$" "$1.words.trigrams" > "$1.wt.clean" &
grep "^[a-zäöüß#][a-zäöüß][a-zäöüß#]$" "$1.single.words.trigrams" > "$1.swt.clean"

wait
## uncomment if there are disk space issues
#rm $1.words.trigrams $1.single.words.trigrams
echo "trigrams cleaned"

# sort, count and format as .csv file
sort -T /media/jph/linuxOnlyData/tmp < "$1.swt.clean" | uniq -c | sed 's/^\s*//' | sed 's/ /,/' > "$1.swtc.csv" &

sort -T /media/jph/linuxOnlyData/tmp < "$1.wt.clean" | uniq -c | sed 's/^\s*//' | sed 's/ /,/' > "$1.wtc.csv"
