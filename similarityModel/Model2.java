package Model2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class Model2 {


	public static LinkedList<String> generateLetterSequences(String s){
		LinkedList<String> letterSequences = new LinkedList<String>();
		for(int n = 0; n <= s.length()-3; n++){

			int[] emptyLetters = new int[n];

			for(int i = 0; i < Math.pow(s.length(), n); i++){

				for(int j = 0; j < n; j++){
					if(j == 0){
						emptyLetters[0] = i%s.length();
					}
					else{
						emptyLetters[j] = i%((int)Math.pow(s.length(), j+1))/((int)Math.pow(s.length(), j));
					}
				}

				StringBuilder newString = new StringBuilder(s);
				for(int h = 0; h < n; h++){
					newString.setCharAt(emptyLetters[h], '#');
				}

				if(!contains(letterSequences, newString.toString())){
					letterSequences.add(newString.toString());
				}

			}
		}
		return letterSequences;
	}


	public static double compareWords(String pseudoWord, String realWord, LinkedList<String> letterSequences){

		int max = 0;

		for(String ls : letterSequences){
			for(int i = 0; i < realWord.length() ; i++){
				boolean doABreak = false;
				int h = 0;
				int counter = 0;
				for(int j = i; j < realWord.length() && h < ls.length() && !doABreak; j++){
					if(ls.charAt(h) != '#'){
						if(ls.charAt(h) == realWord.charAt(j)){
							counter++;
							h++;
						}
						else{
							doABreak= true;
						}
					}
					else{
						h++;
					}
				}
				if(!doABreak){
					if(counter > max){
						max = counter;
					}

				}
			}
		}
		return ((double)max)/((double)pseudoWord.length()) * ((double)max)/((double)realWord.length());
	}

	public static double calculateEstimation(String pseudoWord, LinkedList<String> realWords){
		LinkedList<String> letterSequences = generateLetterSequences(pseudoWord);
		//System.out.println("Done generating letterSequences");
		double currentMax = Double.NEGATIVE_INFINITY;

		for(String realWord : realWords){
			double value = compareWords(pseudoWord, realWord, letterSequences);
			if(value > currentMax){
				currentMax = value;
			}
		}


		return currentMax;
	}

	public static boolean contains(LinkedList<String> ll, String s){
		for(int i = 0; i < ll.size(); i++){
			if(ll.get(i).equals(s)){
				return true;
			}
		}
		return false;
	}

	public static void print(LinkedList<String> ll){

		for(String s : ll){
			System.out.println(s);
		}
	}

	static void printEstimates(String filename) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		boolean readingOver = false;
		while(!readingOver){
			String line = reader.readLine();
			if(line == null){
				readingOver = true;
			}
			else if(line.length() != 0){
				if(line.charAt(0) == 'D'){
					System.out.println(reader.readLine());
				}
			}
		}
	}

	static LinkedList<LinkedList<String>> readWords(String filename) throws IOException{
		LinkedList<LinkedList<String>> allActivatedWords = new LinkedList<LinkedList<String>>();
		boolean readingOver = false;
		BufferedReader reader = new BufferedReader(new FileReader(filename));

		while(!readingOver){
			LinkedList<String> activatedWords = new LinkedList<String>();
			boolean breakIt = false;
			String line = reader.readLine();

			if(line == null){
				breakIt = true;
				readingOver = true;
			}
			else{
				if(line.length() == 0){
					breakIt = true;
				}
				else{
					//System.out.println(line.substring(1, line.length()));
					activatedWords.add(line.substring(1, line.length()));
				}

			}
			int counterOfBreaks = 0;

			while(!breakIt){
				line = reader.readLine();
				if(line == null){
					breakIt = true;
					readingOver = true;
				}
				else if(line.length() == 0){
					counterOfBreaks++;
					if(counterOfBreaks >= 2){
						breakIt = true;
					}
				}
				else{
					counterOfBreaks = 0;
					boolean readingAWord = false;
					int wordBeginning = 0;
					int wordEnding = 0;
					for(int i = 0; i < line.length(); i++){
						if(!readingAWord){
							if(line.charAt(i) == '"'){
								wordBeginning = i+1;
								readingAWord = true;
							}
						}
						else{
							if(line.charAt(i) == '"'){
								wordEnding = i;
								String word = line.substring(wordBeginning, wordEnding);
								activatedWords.add(word);
								readingAWord = false;
							}
						}

					}

				}
			}
			if(activatedWords.size() > 0){
				allActivatedWords.add(activatedWords);
			}
		}


		return allActivatedWords;
	}


	public static void main(String[] args) throws IOException{

		LinkedList<LinkedList<String>> allWords = readWords("../../activatedWords.txt");
		if(allWords.size() != 100 ){
			throw new Error("Wrong size :O");
		}


		for(LinkedList<String> ll : allWords){
			String pseudo = ll.removeFirst();
			System.out.print(pseudo + ": ");
			System.out.println(calculateEstimation(pseudo, ll));

		}


	}
}
