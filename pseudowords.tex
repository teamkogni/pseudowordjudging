\documentclass[letterpaper]{article}
\usepackage{a3ilike}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{graphicx}
% \usepackage{xcolor}
% \definecolor{dkgreen}{rgb}{0,0.6,0}
% \definecolor{gray}{rgb}{0.5,0.5,0.5}
% \definecolor{mauve}{rgb}{0.58,0,0.82}
% \usepackage{listings}
% \lstset{numbers=left,
%     numberstyle=\tiny,
%     numbersep=5pt,
%     breaklines=true,
%     showstringspaces=false,
%     frame=l ,
%     xleftmargin=15pt,
%     xrightmargin=15pt,
%     basicstyle=\ttfamily\scriptsize,
%     stepnumber=1,
%     keywordstyle=\color{blue},          % keyword style
%     commentstyle=\color{dkgreen},       % comment style
%     stringstyle=\color{mauve},         % string literal style
%     columns=flexible,
%     keepspaces=true
% }

\frenchspacing
\setlength{\pdfpagewidth}{8.5in}
\setlength{\pdfpageheight}{11in}
\pdfinfo{
/Title (Model comparison: Judging of German Pseudowords)
/Author (Christian Gumbsch, Jan-Peter Hohloch, Simon Schlegel)}
\setcounter{secnumdepth}{0}

\newcommand{\modelOne}{Top-Activation}
\newcommand{\modelTwo}{Top-Similarity}
\newcommand{\modelThree}{Trigram-frequency in real words}
\newcommand{\modelFour}{Trigram-frequency in language}
\newcommand{\codeurl}{https://bitbucket.org/teamkogni/pseudowordjudging}


\begin{document}
    % The file aaai.sty is the style file for AAAI Press
    % proceedings, working notes, and technical reports.

    \title{Model comparison:\\Judging of German Pseudowords}
    \author{Christan Gumbsch , Jan-Peter Hohloch , Simon Schlegel\\
        Eberhard-Karls-Univesit\"at T\"ubingen}
    \maketitle

    \begin{abstract}
    \begin{quote}
       Everybody can decide if a pseudoword looks language like or not. Here we present four different computational approaches to solve this problem for German pseudowords. We used a weight matrix, which gives us the activation of words by trigrams, and a corpus of German text to train our models. Two of our models work with trigram frequency, one with trigram activation and the last one with letter similarity of words activated by the trigrams. We also launched a survey in which the participants were supposed to rate 100 pseudowords on a scale from one to seven, depending on how German they thought the word looks. After we calculated a score for each pseudoword, we gave each of our models the same 100 words as input and compared the outcome for each word with the score we calculated from the human data. Furthermore the models have been compared among themselves. It shows that the letter similarity based model was giving the best results, even though the corpus based models also did well.
    \end{quote}
    \end{abstract}

    \section{Introduction}
    Humans can decide easily whether a word sounds or looks like it belongs to their mother tongue, but not everybody decides the same way. Therefore it seems unlikely that there is only one major mechanism helping to distinguish between words and pseudowords of a certain language. Presumably a lot of different methods to solve this problem exist and we are not using only one of them to classify a word as suitable for a language. Hence we build four different models each with a different approach, but the main focus of attention is on  word trigrams. Three of the models work with trigrams and the fourth uses letter similarity of trigram activated words. We then compared these models among themselves and with data we collected from native German speakers through a short survey, to find out which model comes the closest to the human evaluations.

    \section{Materials and Methods}
        %Our models
        \subsection{Material}
            \label{sub:material}
            We received some material to train our models and to find good pseudowords \footnote{Material received from Cyrus Shaoul, Quantitative Linguistics group, University of Tuebingen}.
            \begin{enumerate}
            \item \textbf{Weight Matrix:} A matrix containing 10678 words as columns and their trigrams (68076) as rows. Provides the information how heavily a word is activated by a trigram.
            \item \textbf{German Text Corpus:} A corpus of German sample text, containing 697,249,737 words. We had to make some adjustments to the text, since it contained a number of unneeded or distracting characters. We had nearly 30,000 unique trigrams in the end.
            \item \textbf{Pseudoword List:} A list of 1751 pseudowords from which we took 100 randomly for our survey and to test our models.
            \end{enumerate}
            We launched a survey, which 26 native German speakers participated in, containing 100 of the pseudowords from the list of pseudowords. The survey was answered between the 7th and 25th of July 2014. The participants were supposed to rate each pseudoword on a scale from 1 to 7, depending on how German they thought the word looked, at which 7 stands for ``very German'' and 1 meaning ``not German at all''.

        \subsection{Model 1: \modelOne}
            The basic idea for this model is to split a pseudoword up in its trigrams and find the twenty most activated words for each trigram, which can be found in the weight matrix. We then calculate the estimation for a pseudoword $w$ by the mean sum of the activations:
            \begin{equation}
            estimation(w) = \frac{\sum_{T} \sum_{i=1}^{20} activation_T(word_i)}{\#T}
            \end{equation}
            with $T$: the set of trigrams in the pseudoword $w$ and \\ $word_i$: the i-th most activated word. \\
            In the last step the calculated estimations have to be scaled.
        \subsection{Model 2: \modelTwo}
            For this model our aim is to find existing German words which are similar to the pseudoword. Based on the biggest similarity between a real word and the pseudoword we compute the estimation.\\
            Therefore we generate patterns of our pseudowords. A pattern of a pseudoword consists of a specified amount of placeholders at different positions in the word (for example the pseudoword `eic' generates the patterns `eic', `\#ic', `e\#c', `ei\#', `\#\#c', `\#i\#', `e\#\#' and `\#\#\#', with \# a placeholder). To recognize whether a symbol is a letter or a placeholder we formulate a function $placeholder(x)$:

            \begin{equation}
                placeholder(x) =\begin{cases}
                    1, & \text{if }x\text{ is a placeholder}\\
                    0, & \text{else}
               \end{cases}
            \end{equation}
            For the estimation of a pseudoword we look for the pattern with the least amount of placeholders, which fits a substring of an existing word. A pattern fits a substring, if at every position the letter of the pattern and the substring are equal or the pattern contains a placeholder. Formally this can be described by the function $fit(p,y)$ for a pattern $p$ and a word $y$.

            \begin{equation}
                fit(p, y) =\begin{cases}
                    1, & \begin{split}
                    \text{if }\exists i: \forall_{0\leq j \leq length(p)} p[j] = y[i+j] \\ \lor placeholder(p[j])
                    \end{split}\\
                   0, & \text{else}
               \end{cases}
            \end{equation}
            To determine the amount of letters in a pattern $p$ we define the function $letters(p)$:
            \begin{equation}
                   letters(p) = \sum_{0 \leq i \leq length(p)} (1 - placeholder(p[i]))
            \end{equation}
            We can use these functions to compute the estimation. For the estimation of a pseudoword we use the pattern with the least amount of placeholders, which fits a substring of an existing word in a set of German words. The estimation is the maximal percentage of letters of a real word fit by a pattern, multiplied with the percentage of the letters of the pseudoword used in this pattern. Let $w$ be the pseudoword, $P(w)$ the set of patterns generated from $w$ and $Y$ be the set of German words we compare the pseudoword to.
            \begin{equation}
                   estimation(w) = \max_{\forall y \in Y} (\frac{\max_{\forall p \in P(w)} (letters(p) fit(p, y))^2}{length(y) length(w)})
            \end{equation}
            For the set of words for comparison, we again use the twenty most activated words per trigram of the pseudoword, because we assume that these words are most likely to contain long substrings similar to the pseudoword.
        \subsection{Model 3: \modelThree}
            In contrast to the first and second model, for the third and fourth model we use trigram frequency in a corpus of German.\\
            The first approach here is to take every unique word in the corpus, split it into trigrams and count them. This models the possible approach to check all words ever seen - based on trigrams - whether they are (partially) similar to the pseudoword. If there are many words containing the trigrams of the pseudoword it is likely to be a real word, if not it's probably a non-word.
            \begin{equation}
            estimation(w) = \log{\frac{\sum_{t=trigrams(w)}{trigram\_count(t)}}{\#trigrams(w)}}
            \end{equation}
            where $trigram\_count(t)$ is the number of occurrences of trigram $t$ in the German text corpus without duplicate words.
        \subsection{Model 4: \modelFour}
            This model works similar to the third. However we count all trigrams in the corpus, so words that occur often are weighted higher. This models the behaviour to compare the seen pseudoword to the whole language. The availability heuristic plays a role in this, so words that occur more often come to mind faster and are compared with a higher weight.
            \begin{equation}
            estimation(w) = \log{\frac{\sum_{t=trigrams(w)}{trigram\_count(t)}}{\#trigrams(w)}}
            \end{equation}
            where $trigram\_count(t)$ is the number of occurrences of trigram $t$ in the whole German text corpus (with duplicates).
        \subsection{Scaling}
            For scaling we take the scale from 1 to 7 like in the survey. To shrink the outputs of our models to this scale we use the following formula,
            with x the z-transformed ratings of the models:
            \begin{equation}
            x'=\frac{(x-\min(x)) * 6}{\max(x)-\min(x)}+1
            \end{equation}
            For the corpus based models we also apply the $\log$ of the counts, because there are many trigrams with a low rating and a few with extraordinary high counts.
    \section{Results}
        The first model's outcomes were as bad that we didn't go further with any comparison to the other models. All activations were near 0 beside some special cases. Scaling with logarithms for example didn't help because the main part of the estimations were to close together.\\
        For comparison of the remaining models we used three mean differences between estimation and survey data:
        \begin{itemize}
            \item pure mean difference: $mean(diff)$
            \item mean absolute difference: $mean(abs(diff))$
            \item mean corrected absolute difference:\\
                $mean(abs(diff-mean(diff)))$
        \end{itemize}
        We got:\\
        $\begin{array}{c|c|c|c}
            &M2&M3&M4\\\hline
            mean(diff)&0.030&1.377&1.587\\
            mean(abs(diff))&0.918&1.662&1.846\\
            mean(abs(corrDiff))&0.916&1.083&1.134
        \end{array}$\\[0.2cm]
       	In addition, for better understanding of the data, we drew boxplots for each kind of difference:\\
        \includegraphics[width=0.5\textwidth, height=0.5\textwidth]{boxplots.png}

    \section{Discussion}
    As can be seen in the results, the `top similarity' model produced the smallest mean differences of $0.03, 0.91$ and $0.91$ showing that it approximates the human estimation of pseudowords best. Therefore it seems likely that one of the bigger involved methods to decide whether a pseudoword looks German or not, is the comparison of the pseudoword to existing German words. However models 3 and 4 also gave good results with mean differences of $1.37, 1.62$ and $1.08$ for model 3 and $1.58, 1.84$ and $1.13$ for model 4. Thus the mechanism of splitting the pseudowords, in our case in trigrams, and using the frequency of the parts, either in a list of words (model 3) or in the whole language (model 4), seem to influence the decision process as well. The comparison of model 3 and 4 suggests that native speakers check words they know instead of checking their whole language for occurrence of the word parts preferably.
    \section{Acknowledgements}
    	We'd like to thank Cyrus Shaoul for helping us with material and advise as well as every participant of our survey.
    \section{Appendix}
    \subsection{Code} % (fold)
    \label{app:code}
        All the Code is published at\\ \codeurl
        \subsubsection{Weight matrix operations} % (fold)
        \label{app:wm_operations}
            For the operations on the weight matrix (.rds) we used the R programming language as learned in the lecture. Produced output was piped into text files for further computation of model 2.
        % subsection wm_operations (end)
        \subsubsection{Pattern computation} % (fold)
        \label{app:placeholder}
         For $x$ placeholders, we generate every possible combination of the $n$ positions every placeholder can be at. The different patterns contain all placeholder combinations for $x =1, ..., n -3$ , where $n$ is the length of the pseudoword. We used $n-3$ as upper limit because our list of words for comparison is generated based on the trigram frequency, so a substring with at least 3 letters is always found.
        % subsection placeholder (end)
        \subsubsection{Corpus Computation} % (fold)
        \label{app:corpus_computation}
            The biggest problem here was dealing with the size of the corpus. This can be handled quite well by working with bash scripts on streams and getting rid of data that'll never be used but would take computation time nevertheless. In addition we used parallel execution of commands where possible.
        % subsection corpus_computation (end)
        \subsubsection{Models} % (fold)
        \label{app:models}
            The models were calculated in R for easy visualization of data and to use the skills learned in the lecture. The R-Code is also published at \codeurl.
        % subsection models (end)
    % section code (end)
    \subsection{Given Data} % (fold)
    \label{app:given_data}
        For explanation see ``Material and Methods''.\\
        Unfortunately we can't publish the weight matrix and the corpus due to their size.
    % section given_data (end)
\end{document}
